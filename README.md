## To run :
- `mvn clean install`
- `java -jar ./target/treasure-hunt-1.0-SNAPSHOT.jar`

## Default behavior :
- Input file path : `src/main/resources/inputs/map1.txt`
- Output file path : `outputFile.txt` (in root directory)

## command arguments :
- change input file path : "-i" (or "--input") <input_file_path>
- change output file path : "-o" (or "--output") <output_file_path>
- example : `java -jar ./target/treasure-hunt-1.0-SNAPSHOT.jar -i src/main/resources/inputs/map1.txt -o outputFile.txt`

## Program return :
- String value of the input map
- String value of the result map
- Result file path
