package fr.carbonit;

import fr.carbonit.exceptions.*;
import fr.carbonit.services.app.AppService;
import fr.carbonit.services.app.AppServiceImpl;
import fr.carbonit.services.build.BuilderService;
import fr.carbonit.services.build.BuilderServiceImpl;
import fr.carbonit.services.file.FileService;
import fr.carbonit.services.file.FileServiceImpl;
import fr.carbonit.services.move.MoveService;
import fr.carbonit.services.move.MoveServiceImpl;
import fr.carbonit.services.validate.ValidateService;
import fr.carbonit.services.validate.ValidateServiceImpl;
import fr.carbonit.utils.IConstants;

import java.nio.file.Path;

public class App {
    public static void main(String[] args) throws MoveNotFoundException, OrientationNotFoundException, ElementOutOfBoundsException, InvalidInputFileException, MountainCollisionException, OutputFileCreationException, AdventurerNotAloneException {
        String inputFile = null;
        String outputFile = null;

        FileService fileService = new FileServiceImpl();
        ValidateService validateService = new ValidateServiceImpl();
        BuilderService builderService = new BuilderServiceImpl(validateService);
        MoveService moveService = new MoveServiceImpl(validateService);
        AppService appService = new AppServiceImpl(fileService, moveService, builderService);

        for (int i = 0; i < args.length; i++) {
            if (i + 1 >= args.length) {
                break;
            }
            if ("-i".equals(args[i]) || "--input".equals(args[i])) {
                inputFile = args[++i];
            } else if ("-o".equals(args[i]) || "--output".equals(args[i])) {
                outputFile = args[++i];
            } else {
                throw new IllegalArgumentException(String.format(IConstants.EXCEPTION_MESSAGE_INVALID_ARGUMENT, args[i]));
            }
        }

        Path outputPath = appService.run(inputFile, outputFile);
        System.out.println(outputPath);
    }
}
