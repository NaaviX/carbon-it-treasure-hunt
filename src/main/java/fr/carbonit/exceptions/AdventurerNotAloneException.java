package fr.carbonit.exceptions;

import fr.carbonit.models.Adventurer;
import fr.carbonit.utils.IConstants;

public class AdventurerNotAloneException extends CarbonException {
    public AdventurerNotAloneException(Adventurer adventurer, Adventurer adventurer2) {
        super(IConstants.EXCEPTION_MESSAGE_ADVENTURER_NOT_ALONE.formatted(adventurer.getName(), adventurer2.getName(), adventurer.getCoordinates()));
    }
}
