package fr.carbonit.exceptions;

public abstract class CarbonException extends Exception {

    public CarbonException(String message) {
        super(message);
    }

    public CarbonException(Throwable cause) {
        super(cause);
    }
}
