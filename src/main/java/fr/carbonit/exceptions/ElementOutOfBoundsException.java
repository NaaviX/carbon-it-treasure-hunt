package fr.carbonit.exceptions;

import fr.carbonit.models.Element;
import fr.carbonit.utils.IConstants;

public class ElementOutOfBoundsException extends CarbonException {

    public ElementOutOfBoundsException(Element element) {
        super(IConstants.EXCEPTION_MESSAGE_ELEMENT_OUT_OF_BOUNDS.formatted(element.getType().toString(), element.getCoordinates()));
    }
}
