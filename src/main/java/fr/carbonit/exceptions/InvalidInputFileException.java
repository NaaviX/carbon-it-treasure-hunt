package fr.carbonit.exceptions;

import fr.carbonit.utils.IConstants;

public class InvalidInputFileException extends CarbonException {
    public InvalidInputFileException(String msg) {
        super(IConstants.EXCEPTION_MESSAGE_INVALID_INPUT_FILE + " : " + msg);
    }

    public InvalidInputFileException() {
        super(IConstants.EXCEPTION_MESSAGE_INVALID_INPUT_FILE);
    }
}
