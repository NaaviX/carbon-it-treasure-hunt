package fr.carbonit.exceptions;

import fr.carbonit.models.Element;
import fr.carbonit.utils.IConstants;

public class MountainCollisionException extends CarbonException {
    public MountainCollisionException(Element element) {
        super(IConstants.EXCEPTION_MESSAGE_MOUNTAIN_ON_SQUARE.formatted(element.getType().name(), element.getCoordinates()));
    }
}
