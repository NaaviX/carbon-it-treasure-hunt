package fr.carbonit.exceptions;

import fr.carbonit.utils.IConstants;

public class MoveNotFoundException extends CarbonException {
    public MoveNotFoundException(char value) {
        super(IConstants.EXCEPTION_MESSAGE_MOVE_NOT_FOUND.formatted(value));
    }
}
