package fr.carbonit.exceptions;

import fr.carbonit.utils.IConstants;

public class NoMapException extends InvalidInputFileException {
    public NoMapException() {
        super(IConstants.EXCEPTION_MESSAGE_NO_MAP);
    }
}
