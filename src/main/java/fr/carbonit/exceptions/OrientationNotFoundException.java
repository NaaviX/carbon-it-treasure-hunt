package fr.carbonit.exceptions;

import fr.carbonit.utils.IConstants;

public class OrientationNotFoundException extends CarbonException {
    public OrientationNotFoundException(char value) {
        super(IConstants.EXCEPTION_MESSAGE_ORIENTATION_NOT_FOUND.formatted(value));
    }
}
