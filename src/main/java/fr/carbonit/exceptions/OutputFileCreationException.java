package fr.carbonit.exceptions;

import java.io.IOException;

public class OutputFileCreationException extends CarbonException {
    public OutputFileCreationException(IOException e) {
        super(e);
    }
}
