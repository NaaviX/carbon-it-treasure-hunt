package fr.carbonit.models;

import fr.carbonit.models.enums.LineType;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import fr.carbonit.utils.Utils;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Adventurer extends Element {
    public static final LineType TYPE = LineType.ADVENTURER;
    private String name;
    private OrientationType orientation;
    private List<MoveType> moves;
    private int treasuresCount;

    public Adventurer(@NonNull Coordinate coordinates, @NonNull String name, @NonNull OrientationType orientation) {
        super(TYPE, coordinates);
        this.name = name;
        this.orientation = orientation;
        this.moves = new ArrayList<>();
        this.treasuresCount = 0;
    }

    @Override
    public String toString() {
        StringBuilder movesStr = new StringBuilder();
        if (!moves.isEmpty()) {
            moves.forEach(movesStr::append);
        } else {
            movesStr.append(treasuresCount);
        }
        return Utils.formatLine(type.toString(), name, coordinates.toString(), orientation.toString(), movesStr.toString());
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && name.equals(((Adventurer) obj).getName()) && orientation.equals(((Adventurer) obj).getOrientation()) && treasuresCount == ((Adventurer) obj).getTreasuresCount() && moves.equals(((Adventurer) obj).getMoves());
    }
}
