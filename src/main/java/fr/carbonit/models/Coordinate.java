package fr.carbonit.models;

import fr.carbonit.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Coordinate {
    protected int x;
    protected int y;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Coordinate && ((Coordinate) obj).getX() == this.x && ((Coordinate) obj).getY() == this.y;
    }

    @Override
    public String toString() {
        return Utils.formatLine(String.valueOf(x), String.valueOf(y));
    }
}
