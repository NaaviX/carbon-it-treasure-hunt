package fr.carbonit.models;

import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class Element {
    protected LineType type;
    protected Coordinate coordinates;

    @Override
    public String toString() {
        return Utils.formatLine(type.toString(), coordinates.toString());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Element && type.equals(((Element) obj).getType()) && coordinates.equals(((Element) obj).getCoordinates());
    }
}
