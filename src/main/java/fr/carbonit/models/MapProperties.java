package fr.carbonit.models;

import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.IConstants;
import fr.carbonit.utils.Utils;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Getter
@Setter
public class MapProperties implements Cloneable {
    public static final LineType TYPE = LineType.MAP_PROPERTIES;

    private Size size;
    private List<Mountain> mountains;
    private List<Treasure> treasures;
    private List<Adventurer> adventurers;

    public MapProperties(@NonNull Size size) {
        this.size = size;
        this.mountains = new ArrayList<>();
        this.treasures = new ArrayList<>();
        this.adventurers = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(IConstants.NEW_LINE);

        stringJoiner.add(IConstants.COMMENT_LINE_MAP)
                .add(Utils.formatLine(TYPE, size)).add("");

        stringJoiner.add(IConstants.COMMENT_LINE_MOUNTAIN);
        mountains.forEach(mountain -> stringJoiner.add(mountain.toString()));
        stringJoiner.add("");

        stringJoiner.add(IConstants.COMMENT_LINE_TREASURE);
        treasures.forEach(treasure -> stringJoiner.add(treasure.toString()));
        stringJoiner.add("");

        if (!adventurers.isEmpty()) {
            stringJoiner.add(IConstants.COMMENT_LINE_ADVENTURER_INPUT);
        } else {
            stringJoiner.add(IConstants.COMMENT_LINE_ADVENTURER_OUTPUT);
        }

        adventurers.forEach(adventurer -> stringJoiner.add(adventurer.toString()));

        return stringJoiner.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MapProperties) {
            return obj.toString().equals(toString());
        }
        return false;
    }

    @Override
    public MapProperties clone() throws CloneNotSupportedException {
        return (MapProperties) super.clone();
    }
}
