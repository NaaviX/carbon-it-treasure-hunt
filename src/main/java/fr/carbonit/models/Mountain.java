package fr.carbonit.models;

import fr.carbonit.models.enums.LineType;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class Mountain extends Element {
    public static final LineType TYPE = LineType.MOUNTAIN;

    public Mountain(@NonNull Coordinate coordinates) {
        super(LineType.MOUNTAIN, coordinates);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Mountain && coordinates.equals(((Mountain) obj).getCoordinates()) && ((Mountain) obj).getType().equals(LineType.MOUNTAIN);
    }
}
