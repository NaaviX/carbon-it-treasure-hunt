package fr.carbonit.models;

import fr.carbonit.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Size {
    private int width;
    private int height;

    @Override
    public String toString() {
        return Utils.formatLine(String.valueOf(width), String.valueOf(height));
    }
}
