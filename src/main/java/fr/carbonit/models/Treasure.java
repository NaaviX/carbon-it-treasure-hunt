package fr.carbonit.models;

import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.Utils;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class Treasure extends Element {
    public static final LineType TYPE = LineType.TREASURE;
    private int count;

    public Treasure(@NonNull Coordinate coordinates, int count) {
        super(TYPE, coordinates);
        this.count = count;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Treasure && ((Treasure) obj).getCount() == count && ((Treasure) obj).getCoordinates().equals(coordinates);
    }

    @Override
    public String toString() {
        return Utils.formatLine(type.toString(), coordinates.toString(), String.valueOf(count));
    }


}
