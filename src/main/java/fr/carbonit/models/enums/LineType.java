package fr.carbonit.models.enums;

import fr.carbonit.utils.IConstants;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum LineType {
    MAP_PROPERTIES(IConstants.LETTER_MAP_PROPERTIES),
    MOUNTAIN(IConstants.LETTER_MOUNTAIN),
    TREASURE(IConstants.LETTER_TREASURE),
    ADVENTURER(IConstants.LETTER_ADVENTURER),
    COMMENT(IConstants.LETTER_COMMENT);

    private final char value;


    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
