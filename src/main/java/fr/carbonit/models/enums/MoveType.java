package fr.carbonit.models.enums;

import fr.carbonit.exceptions.MoveNotFoundException;
import fr.carbonit.utils.IConstants;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum MoveType {
    FORWARD(IConstants.LETTER_ACTION_FORWARD),
    LEFT(IConstants.LETTER_ACTION_LEFT),
    RIGHT(IConstants.LETTER_ACTION_RIGHT);

    private final char value;

    public static MoveType fromValue(char value) throws MoveNotFoundException {
        MoveType moveType = Arrays.stream(MoveType.values()).filter(m -> m.toString().charAt(0) == value).findFirst().orElse(null);
        if (moveType == null) {
            throw new MoveNotFoundException(value);
        }
        return moveType;
    }


    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
