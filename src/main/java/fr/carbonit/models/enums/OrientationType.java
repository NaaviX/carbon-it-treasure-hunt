package fr.carbonit.models.enums;

import fr.carbonit.exceptions.OrientationNotFoundException;
import fr.carbonit.utils.IConstants;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum OrientationType {
    NORTH(IConstants.LETTER_ORIENTATION_NORTH),
    SOUTH(IConstants.LETTER_ORIENTATION_SOUTH),
    EAST(IConstants.LETTER_ORIENTATION_EAST),
    WEST(IConstants.LETTER_ORIENTATION_WEST);

    private final char value;

    public static OrientationType fromValue(char value) throws OrientationNotFoundException {
        OrientationType orientation = Arrays.stream(OrientationType.values()).filter(o -> o.toString().charAt(0) == value).findFirst().orElse(null);
        if (orientation == null) {
            throw new OrientationNotFoundException(value);
        }
        return orientation;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
