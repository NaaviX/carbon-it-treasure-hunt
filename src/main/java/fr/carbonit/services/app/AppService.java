package fr.carbonit.services.app;

import fr.carbonit.exceptions.*;

import java.nio.file.Path;

public interface AppService {
    Path run(String inputFile, String outputFile) throws InvalidInputFileException, MoveNotFoundException, OrientationNotFoundException, ElementOutOfBoundsException, MountainCollisionException, OutputFileCreationException, AdventurerNotAloneException;
}
