package fr.carbonit.services.app;

import fr.carbonit.exceptions.*;
import fr.carbonit.models.MapProperties;
import fr.carbonit.services.build.BuilderService;
import fr.carbonit.services.file.FileService;
import fr.carbonit.services.move.MoveService;
import fr.carbonit.utils.IConstants;
import lombok.RequiredArgsConstructor;

import java.nio.file.Path;
import java.util.List;

@RequiredArgsConstructor
public class AppServiceImpl implements AppService {
    private final FileService fileService;
    private final MoveService moveService;
    private final BuilderService builderService;

    @Override
    public Path run(String inputFile, String outputFile) throws InvalidInputFileException, MoveNotFoundException, OrientationNotFoundException, ElementOutOfBoundsException, MountainCollisionException, OutputFileCreationException, AdventurerNotAloneException {
        List<String> mapLines = fileService.getLinesFromFile(inputFile);
        MapProperties mapProperties = builderService.buildMapProperties(mapLines);
        System.out.println(mapProperties.toString().concat(IConstants.NEW_LINE));
        mapProperties = moveService.playMoves(mapProperties);
        System.out.println(mapProperties.toString().concat(IConstants.NEW_LINE));
        return fileService.writeObjectsToFile(outputFile, mapProperties);
    }
}
