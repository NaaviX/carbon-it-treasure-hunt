package fr.carbonit.services.build;

import fr.carbonit.exceptions.*;
import fr.carbonit.models.MapProperties;

import java.util.List;

public interface BuilderService {
    MapProperties buildMapProperties(List<String> mapLines) throws MoveNotFoundException, OrientationNotFoundException, ElementOutOfBoundsException, MountainCollisionException, InvalidInputFileException, AdventurerNotAloneException;
}
