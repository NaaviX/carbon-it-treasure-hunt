package fr.carbonit.services.build;

import fr.carbonit.exceptions.*;
import fr.carbonit.models.Adventurer;
import fr.carbonit.models.MapProperties;
import fr.carbonit.models.Mountain;
import fr.carbonit.models.Treasure;
import fr.carbonit.services.validate.ValidateService;
import fr.carbonit.utils.IConstants;
import fr.carbonit.utils.Mapper;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class BuilderServiceImpl implements BuilderService {

    private final ValidateService gameValidateService;

    @Override
    public MapProperties buildMapProperties(List<String> mapLines) throws MoveNotFoundException, OrientationNotFoundException, ElementOutOfBoundsException, MountainCollisionException, InvalidInputFileException, AdventurerNotAloneException {
        MapProperties mapProperties = null;
        List<Adventurer> adventurers = new ArrayList<>();
        List<Mountain> mountains = new ArrayList<>();
        List<Treasure> treasures = new ArrayList<>();

        for (String line : mapLines) {
            String[] lineArgs = line.split(IConstants.LINE_DELIMITER);
            switch (lineArgs[0].charAt(0)) {
                case IConstants.LETTER_MAP_PROPERTIES -> mapProperties = Mapper.mapLineToMapProperties(lineArgs);
                case IConstants.LETTER_ADVENTURER -> adventurers.add(Mapper.mapLineToAdventurer(lineArgs));
                case IConstants.LETTER_MOUNTAIN -> mountains.add(Mapper.mapLineToMountain(lineArgs));
                case IConstants.LETTER_TREASURE -> treasures.add(Mapper.mapLineToTreasure(lineArgs));
            }
        }

        if (mapProperties == null) {
            throw new NoMapException();
        }
        mapProperties.getMountains().addAll(mountains);
        mapProperties.getTreasures().addAll(treasures);
        mapProperties.getAdventurers().addAll(adventurers);

        gameValidateService.validateMap(mapProperties);

        return mapProperties;
    }
}
