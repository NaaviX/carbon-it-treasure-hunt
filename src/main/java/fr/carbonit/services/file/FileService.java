package fr.carbonit.services.file;

import fr.carbonit.exceptions.InvalidInputFileException;
import fr.carbonit.exceptions.OutputFileCreationException;
import fr.carbonit.models.MapProperties;

import java.nio.file.Path;
import java.util.List;

public interface FileService {
    List<String> getLinesFromFile(String inputFileName) throws InvalidInputFileException;

    Path writeObjectsToFile(String outputFile, MapProperties mapProperties) throws OutputFileCreationException;
}
