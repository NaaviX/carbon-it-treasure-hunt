package fr.carbonit.services.file;

import fr.carbonit.exceptions.InvalidInputFileException;
import fr.carbonit.exceptions.OutputFileCreationException;
import fr.carbonit.models.MapProperties;
import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.IConstants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class FileServiceImpl implements FileService {
    @Override
    public List<String> getLinesFromFile(String inputFileName) throws InvalidInputFileException {
        if (inputFileName == null || inputFileName.isEmpty()) {
            inputFileName = IConstants.DEFAULT_INPUT_FILE_PATH;
        }
        Path inputFilePath = Paths.get(inputFileName);
        try (Stream<String> linesStream = Files.lines(inputFilePath)) {
            return linesStream.filter(s -> s != null && !s.isEmpty() && !s.startsWith(LineType.COMMENT.toString())).toList();
        } catch (Exception e) {
            throw new InvalidInputFileException(e.getMessage());
        }
    }

    @Override
    public Path writeObjectsToFile(String outputFile, MapProperties mapProperties) throws OutputFileCreationException {
        if (outputFile == null || outputFile.isEmpty()) {
            outputFile = IConstants.DEFAULT_OUTPUT_FILE_PATH;
        }
        try {
            Path filePath = Paths.get(outputFile);
            Files.deleteIfExists(filePath);
            Files.createFile(filePath);
            return Files.writeString(filePath, mapProperties.toString());
        } catch (IOException e) {
            throw new OutputFileCreationException(e);
        }
    }
}
