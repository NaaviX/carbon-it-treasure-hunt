package fr.carbonit.services.move;

import fr.carbonit.models.MapProperties;

public interface MoveService {
    MapProperties playMoves(MapProperties mapProperties);
}
