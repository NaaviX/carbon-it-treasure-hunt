package fr.carbonit.services.move;

import fr.carbonit.exceptions.AdventurerNotAloneException;
import fr.carbonit.exceptions.ElementOutOfBoundsException;
import fr.carbonit.exceptions.MountainCollisionException;
import fr.carbonit.models.Adventurer;
import fr.carbonit.models.Coordinate;
import fr.carbonit.models.MapProperties;
import fr.carbonit.models.Treasure;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import fr.carbonit.services.validate.ValidateService;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class MoveServiceImpl implements MoveService {

    private final ValidateService gameValidateService;

    @Override
    public MapProperties playMoves(MapProperties mapProperties) {
        List<Adventurer> adventurers = mapProperties.getAdventurers();
        int nbPlays = adventurers.stream().map(adventurer -> adventurer.getMoves().size()).reduce(Integer::max).orElse(0);
        for (int i = 0; i < nbPlays; i++) {
            for (Adventurer adventurer : adventurers) {
                List<MoveType> moves = adventurer.getMoves();
                if (moves.isEmpty()) {
                    continue;
                }
                switch (moves.get(i)) {
                    case MoveType.FORWARD -> moveForward(adventurer, mapProperties);
                    case MoveType.RIGHT -> turnRight(adventurer);
                    case MoveType.LEFT -> turnLeft(adventurer);
                }
                if (moves.size() - 1 == i) {
                    adventurer.getMoves().clear();
                }
            }
        }
        return mapProperties;
    }

    private void moveForward(Adventurer adventurer, MapProperties mapProperties) {
        OrientationType orientation = adventurer.getOrientation();
        Coordinate prevCoordinates = adventurer.getCoordinates();
        Coordinate nextCoordinates = getNextCoordinates(adventurer, orientation);
        adventurer.setCoordinates(nextCoordinates);
        try {
            gameValidateService.validateAdventurerPosition(mapProperties, adventurer);
        } catch (ElementOutOfBoundsException | MountainCollisionException | AdventurerNotAloneException e) {
            adventurer.setCoordinates(prevCoordinates);
            return;
        }
        openTreasure(mapProperties, adventurer);
    }

    private void openTreasure(MapProperties mapProperties, Adventurer adventurer) {
        Treasure treasureOnSquare = mapProperties.getTreasures().stream().filter(treasure -> treasure.getCoordinates().equals(adventurer.getCoordinates())).findFirst().orElse(null);

        if (treasureOnSquare != null) {
            int nextCount = treasureOnSquare.getCount() - 1;
            if (nextCount == 0) {
                mapProperties.getTreasures().remove(treasureOnSquare);
            } else {
                treasureOnSquare.setCount(nextCount);
            }
            adventurer.setTreasuresCount(adventurer.getTreasuresCount() + 1);
        }

    }

    private Coordinate getNextCoordinates(Adventurer adventurer, OrientationType orientation) {
        Coordinate coordinates = adventurer.getCoordinates();
        int xPos = coordinates.getX();
        int yPos = coordinates.getY();
        return switch (orientation) {
            case OrientationType.SOUTH -> new Coordinate(xPos, yPos + 1);
            case OrientationType.EAST -> new Coordinate(xPos + 1, yPos);
            case OrientationType.WEST -> new Coordinate(xPos - 1, yPos);
            case OrientationType.NORTH -> new Coordinate(xPos, yPos - 1);
        };
    }

    private void turnLeft(Adventurer adventurer) {
        OrientationType newOrientation = switch (adventurer.getOrientation()) {
            case OrientationType.SOUTH -> OrientationType.EAST;
            case OrientationType.EAST -> OrientationType.NORTH;
            case OrientationType.WEST -> OrientationType.SOUTH;
            case OrientationType.NORTH -> OrientationType.WEST;
        };
        adventurer.setOrientation(newOrientation);
    }

    private void turnRight(Adventurer adventurer) {
        OrientationType newOrientation = switch (adventurer.getOrientation()) {
            case OrientationType.SOUTH -> OrientationType.WEST;
            case OrientationType.EAST -> OrientationType.SOUTH;
            case OrientationType.WEST -> OrientationType.NORTH;
            case OrientationType.NORTH -> OrientationType.EAST;
        };
        adventurer.setOrientation(newOrientation);
    }

}
