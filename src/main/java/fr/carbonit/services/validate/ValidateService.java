package fr.carbonit.services.validate;

import fr.carbonit.exceptions.AdventurerNotAloneException;
import fr.carbonit.exceptions.ElementOutOfBoundsException;
import fr.carbonit.exceptions.MountainCollisionException;
import fr.carbonit.models.Adventurer;
import fr.carbonit.models.MapProperties;

public interface ValidateService {
    void validateMap(MapProperties mapProperties) throws ElementOutOfBoundsException, MountainCollisionException, AdventurerNotAloneException;

    void validateAdventurerPosition(MapProperties mapProperties, Adventurer adventurer) throws ElementOutOfBoundsException, MountainCollisionException, AdventurerNotAloneException;
}
