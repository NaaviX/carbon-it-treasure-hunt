package fr.carbonit.services.validate;

import fr.carbonit.exceptions.AdventurerNotAloneException;
import fr.carbonit.exceptions.ElementOutOfBoundsException;
import fr.carbonit.exceptions.MountainCollisionException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.Utils;

import java.util.List;
import java.util.Optional;

public class ValidateServiceImpl implements ValidateService {

    @Override
    public void validateMap(MapProperties mapProperties) throws ElementOutOfBoundsException, MountainCollisionException, AdventurerNotAloneException {
        for (Element element : Utils.getAllElements(mapProperties)) {
            LineType elementType = element.getType();
            switch (elementType) {
                case LineType.ADVENTURER -> validateAdventurerPosition(mapProperties, (Adventurer) element);
                case LineType.TREASURE -> validateTreasurePosition(mapProperties, (Treasure) element);
                default -> validateElementPosition(mapProperties, element);
            }
        }
    }

    @Override
    public void validateAdventurerPosition(MapProperties mapProperties, Adventurer adventurer) throws ElementOutOfBoundsException, MountainCollisionException, AdventurerNotAloneException {
        validateElementPosition(mapProperties, adventurer);
        assertIsAlone(adventurer, mapProperties.getAdventurers());
    }

    private void validateTreasurePosition(MapProperties mapProperties, Treasure treasure) throws ElementOutOfBoundsException, MountainCollisionException {
        validateElementPosition(mapProperties, treasure);
        fixMultipleTreasuresSamePosition(treasure, mapProperties.getTreasures());
    }

    private void validateElementPosition(MapProperties mapProperties, Element element) throws ElementOutOfBoundsException, MountainCollisionException {
        assertIsWithinMapLimits(element, mapProperties.getSize());
        assertIsNotOnMountain(element, mapProperties.getMountains());
    }

    private void fixMultipleTreasuresSamePosition(Treasure treasure, List<Treasure> treasures) {
        if (!treasures.isEmpty())
            treasures.stream().filter(t -> !t.equals(treasure) && isOnSamePosition(t, treasure)).findFirst().ifPresent(t -> {
                t.setCount(t.getCount() + treasure.getCount());
                treasures.remove(treasure);
            });
    }

    private void assertIsWithinMapLimits(Element element, Size mapSize) throws ElementOutOfBoundsException {
        Coordinate coordinate = element.getCoordinates();
        int xPos = coordinate.getX();
        int yPos = coordinate.getY();
        if (mapSize.getWidth() < xPos || mapSize.getHeight() < yPos || xPos < 0 || yPos < 0) {
            throw new ElementOutOfBoundsException(element);
        }
    }

    private void assertIsNotOnMountain(Element element, List<Mountain> mountains) throws MountainCollisionException {
        if (mountains.stream().anyMatch(mountain -> !element.equals(mountain) && isOnSamePosition(element, mountain))) {
            throw new MountainCollisionException(element);
        }
    }

    private void assertIsAlone(Adventurer adventurer, List<Adventurer> adventurers) throws AdventurerNotAloneException {
        Optional<Adventurer> adventurerSamePosition = adventurers.stream().filter(adventurer2 -> !adventurer.equals(adventurer2) && isOnSamePosition(adventurer2, adventurer)).findAny();
        if (adventurerSamePosition.isPresent()) {
            throw new AdventurerNotAloneException(adventurer, adventurerSamePosition.get());
        }
    }

    private boolean isOnSamePosition(Element e1, Element e2) {
        return e1.getCoordinates().equals(e2.getCoordinates());
    }
}
