package fr.carbonit.utils;

public interface IConstants {
    String NEW_LINE = System.lineSeparator();

    char LETTER_MAP_PROPERTIES = 'C';
    char LETTER_ADVENTURER = 'A';
    char LETTER_MOUNTAIN = 'M';
    char LETTER_TREASURE = 'T';
    char LETTER_COMMENT = '#';

    char LETTER_ORIENTATION_SOUTH = 'S';
    char LETTER_ORIENTATION_NORTH = 'N';
    char LETTER_ORIENTATION_EAST = 'E';
    char LETTER_ORIENTATION_WEST = 'W';

    char LETTER_ACTION_FORWARD = 'A';
    char LETTER_ACTION_LEFT = 'G';
    char LETTER_ACTION_RIGHT = 'D';

    String LINE_DELIMITER = " - ";
    String DEFAULT_OUTPUT_FILE_PATH = "outputFile.txt";
    String DEFAULT_INPUT_FILE_PATH = "src/main/resources/inputs/map1.txt";

    String COMMENT_LINE_MAP = "# {C comme Carte} - {Nb. de case en largeur} - {Nb. de case en hauteur}";
    String COMMENT_LINE_MOUNTAIN = "# {M comme Montagne} - {Axe horizontal} - {Axe vertical}";
    String COMMENT_LINE_TREASURE = "# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors}";
    String COMMENT_LINE_ADVENTURER_INPUT = "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}";
    String COMMENT_LINE_ADVENTURER_OUTPUT = "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}";

    String EXCEPTION_MESSAGE_PREFIX = "Erreur : ";
    String EXCEPTION_MESSAGE_ELEMENT_OUT_OF_BOUNDS = EXCEPTION_MESSAGE_PREFIX.concat("L'élément %s placé en (%s) est en dehors des dimensions de la carte.");
    String EXCEPTION_MESSAGE_MOUNTAIN_ON_SQUARE = EXCEPTION_MESSAGE_PREFIX.concat("L'élément de type %s ne peut être placé en (%s) car une montagne s'y trouve.");
    String EXCEPTION_MESSAGE_MOVE_NOT_FOUND = EXCEPTION_MESSAGE_PREFIX.concat("Aucun mouvement trouvé pour la lettre %c");
    String EXCEPTION_MESSAGE_ORIENTATION_NOT_FOUND = EXCEPTION_MESSAGE_PREFIX.concat("Aucune orientation d'aventurier trouvée pour la lettre %c");
    String EXCEPTION_MESSAGE_NO_MAP = EXCEPTION_MESSAGE_PREFIX.concat("Aucune carte n'est présente dans le fichier d'entrée");
    String EXCEPTION_MESSAGE_INVALID_INPUT_FILE = EXCEPTION_MESSAGE_PREFIX.concat("Fichier d'entrée invalide");
    String EXCEPTION_MESSAGE_ADVENTURER_NOT_ALONE = EXCEPTION_MESSAGE_PREFIX.concat("Les aventuriers %s et %s ne peuvent pas être sur la même case (%s).");
    String EXCEPTION_MESSAGE_INVALID_ARGUMENT = EXCEPTION_MESSAGE_PREFIX.concat("Argument inconnu : %s. Tapez -i pour le chemin du fichier d'entrée, et -o pour celui de sortie.");

    String TEST_INPUT_FILE_PATH = "src/test/resources/inputFile.txt";
    String TEST_OUTPUT_FILE_PATH = "src/test/resources/outputFile.txt";
}
