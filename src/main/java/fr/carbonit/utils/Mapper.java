package fr.carbonit.utils;

import fr.carbonit.exceptions.MoveNotFoundException;
import fr.carbonit.exceptions.OrientationNotFoundException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;

import java.util.List;

public class Mapper {
    public static Adventurer mapLineToAdventurer(String[] args) throws OrientationNotFoundException, MoveNotFoundException {
        String name = args[1];
        int x = Integer.parseInt(args[2]);
        int y = Integer.parseInt(args[3]);

        OrientationType orientation = OrientationType.fromValue(args[4].charAt(0));
        List<MoveType> moves = Utils.getMovesFromString(args[5]);

        Coordinate coordinate = new Coordinate(x, y);

        Adventurer adventurer = new Adventurer(coordinate, name, orientation);
        adventurer.getMoves().addAll(moves);
        return adventurer;
    }

    public static Mountain mapLineToMountain(String[] args) {
        int x = Integer.parseInt(args[1]);
        int y = Integer.parseInt(args[2]);

        Coordinate coordinate = new Coordinate(x, y);

        return new Mountain(coordinate);
    }

    public static Treasure mapLineToTreasure(String[] args) {
        int x = Integer.parseInt(args[1]);
        int y = Integer.parseInt(args[2]);
        int count = Integer.parseInt(args[3]);

        Coordinate coordinate = new Coordinate(x, y);

        return new Treasure(coordinate, count);
    }

    public static MapProperties mapLineToMapProperties(String[] args) {
        int width = Integer.parseInt(args[1]);
        int height = Integer.parseInt(args[2]);

        Size size = new Size(width, height);

        return new MapProperties(size);
    }
}
