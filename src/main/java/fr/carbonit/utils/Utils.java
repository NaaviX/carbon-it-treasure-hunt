package fr.carbonit.utils;

import fr.carbonit.exceptions.MoveNotFoundException;
import fr.carbonit.models.Element;
import fr.carbonit.models.MapProperties;
import fr.carbonit.models.enums.MoveType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {
    public static String formatLine(Object... args) {
        return Arrays.stream(args).map(Object::toString).collect(Collectors.joining(IConstants.LINE_DELIMITER));
    }

    public static List<MoveType> getMovesFromString(String actionsString) throws MoveNotFoundException {
        List<MoveType> moves = new ArrayList<>();
        for (char actionValue : actionsString.toCharArray()) {
            moves.add(MoveType.fromValue(actionValue));
        }
        return moves;
    }

    public static List<Element> getAllElements(MapProperties mapProperties) {
        return Stream.of(mapProperties.getAdventurers(), mapProperties.getMountains(), mapProperties.getTreasures()).flatMap(Collection::stream).collect(Collectors.toList());
    }
}
