package fr.carbonit;

import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;

import java.util.List;

public class ExampleMapProperties {

    public static MapProperties getInputMapProperties() {
        MapProperties mapProperties = new MapProperties(new Size(3, 4));
        Mountain mountain1 = new Mountain(new Coordinate(1, 0));
        Mountain mountain2 = new Mountain(new Coordinate(2, 1));
        mapProperties.getMountains().add(mountain1);
        mapProperties.getMountains().add(mountain2);

        Treasure treasure1 = new Treasure(new Coordinate(0, 3), 2);
        Treasure treasure2 = new Treasure(new Coordinate(1, 3), 3);
        mapProperties.getTreasures().add(treasure1);
        mapProperties.getTreasures().add(treasure2);

        Adventurer adventurer = new Adventurer(new Coordinate(1, 1), "Lara", OrientationType.SOUTH);
        adventurer.getMoves().addAll(List.of(MoveType.FORWARD, MoveType.FORWARD, MoveType.RIGHT, MoveType.FORWARD, MoveType.RIGHT, MoveType.FORWARD, MoveType.LEFT, MoveType.LEFT, MoveType.FORWARD));
        mapProperties.getAdventurers().add(adventurer);

        return mapProperties;
    }

    public static MapProperties getOutputMapProperties() {
        MapProperties mapProperties = getInputMapProperties();
        mapProperties.getTreasures().remove(0);
        mapProperties.getTreasures().get(0).setCount(2);

        Adventurer expectedAdventurer = mapProperties.getAdventurers().get(0);
        expectedAdventurer.getMoves().clear();
        expectedAdventurer.setCoordinates(new Coordinate(0, 3));
        expectedAdventurer.setTreasuresCount(3);

        return mapProperties;
    }
}
