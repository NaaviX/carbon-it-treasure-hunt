package fr.carbonit.services.app;

import fr.carbonit.ExampleMapProperties;
import fr.carbonit.models.MapProperties;
import fr.carbonit.services.build.BuilderService;
import fr.carbonit.services.file.FileServiceImpl;
import fr.carbonit.services.move.MoveService;
import fr.carbonit.utils.IConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppServiceImplTest {
    @Mock
    private FileServiceImpl fileService;
    @Mock
    private BuilderService builderService;
    @Mock
    private MoveService moveService;
    @InjectMocks
    private AppServiceImpl appServiceImplUnderTest;

    @BeforeEach
    void setUp() {
    }

    @Test
    void testRun() throws Exception {
        MapProperties mapProperties = ExampleMapProperties.getInputMapProperties();

        List<String> mapLines = List.of(mapProperties.getSize().toString(),
                mapProperties.getMountains().get(0).toString(),
                mapProperties.getMountains().get(1).toString(),
                mapProperties.getTreasures().get(0).toString(),
                mapProperties.getTreasures().get(1).toString(),
                mapProperties.getAdventurers().get(0).toString());

        Path inputFilePath = Paths.get(IConstants.TEST_INPUT_FILE_PATH);
        Path outputFilePath = Paths.get(IConstants.TEST_OUTPUT_FILE_PATH);
        assertTrue(Files.exists(inputFilePath));
        assertEquals(Files.readString(inputFilePath), mapProperties.toString());
        when(fileService.getLinesFromFile(IConstants.TEST_INPUT_FILE_PATH)).thenReturn(mapLines);
        when(builderService.buildMapProperties(mapLines)).thenReturn(mapProperties);

        MapProperties expectedMapProperties = ExampleMapProperties.getOutputMapProperties();
        when(moveService.playMoves(mapProperties)).thenReturn(expectedMapProperties);
        when(fileService.writeObjectsToFile(IConstants.TEST_OUTPUT_FILE_PATH, expectedMapProperties)).thenCallRealMethod();

        Path outputPath = appServiceImplUnderTest.run(IConstants.TEST_INPUT_FILE_PATH, IConstants.TEST_OUTPUT_FILE_PATH);

        verify(fileService, times(1)).getLinesFromFile(anyString());
        verify(builderService, times(1)).buildMapProperties(anyList());
        verify(moveService, times(1)).playMoves(any(MapProperties.class));
        verify(fileService, times(1)).writeObjectsToFile(anyString(), any(MapProperties.class));

        assertEquals(outputFilePath, outputPath);
        assertTrue(Files.exists(outputPath));
        assertEquals(Files.readString(outputPath), expectedMapProperties.toString());
    }
}
