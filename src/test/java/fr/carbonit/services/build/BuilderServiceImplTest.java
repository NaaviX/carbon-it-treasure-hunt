package fr.carbonit.services.build;

import fr.carbonit.ExampleMapProperties;
import fr.carbonit.exceptions.InvalidInputFileException;
import fr.carbonit.exceptions.NoMapException;
import fr.carbonit.models.MapProperties;
import fr.carbonit.services.file.FileService;
import fr.carbonit.services.file.FileServiceImpl;
import fr.carbonit.services.validate.ValidateService;
import fr.carbonit.utils.IConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BuilderServiceImplTest {

    @Mock
    private ValidateService mockGameValidateService;

    @InjectMocks
    private BuilderServiceImpl builderServiceImplUnderTest;

    @Test
    void testBuildMapProperties() throws Exception {
        FileService fileService = new FileServiceImpl();
        List<String> mapLines = fileService.getLinesFromFile(IConstants.TEST_INPUT_FILE_PATH);

        MapProperties result = builderServiceImplUnderTest.buildMapProperties(mapLines);

        assertEquals(ExampleMapProperties.getInputMapProperties(), result);
        verify(mockGameValidateService).validateMap(any(MapProperties.class));
    }

    @Test
    void testBuildMapProperties_withNoMap_shouldThrowNoMapException() throws InvalidInputFileException {
        FileService fileService = new FileServiceImpl();
        List<String> mapLines = fileService.getLinesFromFile("src/test/resources/inputFile_noMap.txt");

        assertThrows(NoMapException.class, () -> builderServiceImplUnderTest.buildMapProperties(mapLines));
    }
}
