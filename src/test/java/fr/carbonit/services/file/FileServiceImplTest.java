package fr.carbonit.services.file;

import fr.carbonit.exceptions.InvalidInputFileException;
import fr.carbonit.exceptions.OutputFileCreationException;
import fr.carbonit.models.MapProperties;
import fr.carbonit.models.Size;
import fr.carbonit.models.enums.LineType;
import fr.carbonit.utils.IConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FileServiceImplTest {
    @InjectMocks
    private FileServiceImpl fileServiceImplUnderTest;

    @Test
    void testGetLinesFromFile_ok() throws InvalidInputFileException {

        List<String> linesFromFile = fileServiceImplUnderTest.getLinesFromFile(IConstants.TEST_INPUT_FILE_PATH);
        assertNotNull(linesFromFile);
        assertFalse(linesFromFile.isEmpty());

        List<String[]> linesFromFileArgs = linesFromFile.stream().map(s -> s.split(IConstants.LINE_DELIMITER)).toList();
        assertTrue(linesFromFile.stream().allMatch(s -> StringUtils.isNotEmpty(s) && !s.startsWith(LineType.COMMENT.toString())));
        assertTrue(linesFromFileArgs.stream().anyMatch(s -> s[0].equals(LineType.MAP_PROPERTIES.toString()) && s.length == 3 && Integer.parseInt(s[1]) > 0 && Integer.parseInt(s[2]) > 0));
    }

    @Test
    void testGetLinesFromFile_withInvalidFilePath_throwsInvalidInputFileException() {
        final String inputFileName = "unknownFile.txt";

        assertThrows(InvalidInputFileException.class, () -> fileServiceImplUnderTest.getLinesFromFile(inputFileName));
    }

    @Test
    void testGetLinesFromFile_withEmptyFilePath_throwsInvalidInputFileException() {
        final String inputFileName = "";

        assertThrows(InvalidInputFileException.class, () -> fileServiceImplUnderTest.getLinesFromFile(inputFileName));
    }

    @Test
    void testWriteObjectsToFile() throws OutputFileCreationException, IOException {
        final MapProperties mapProperties = new MapProperties(new Size(3, 4));

        fileServiceImplUnderTest.writeObjectsToFile(IConstants.TEST_OUTPUT_FILE_PATH, mapProperties);
        Path outputFilePath = Paths.get(IConstants.TEST_OUTPUT_FILE_PATH);

        //Assert output file created
        assertTrue(Files.exists(outputFilePath));

        //Assert mapProperties correctly written
        assertEquals(Files.readString(outputFilePath), mapProperties.toString());
    }
}
