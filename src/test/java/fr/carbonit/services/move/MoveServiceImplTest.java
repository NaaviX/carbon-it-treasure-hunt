package fr.carbonit.services.move;

import fr.carbonit.exceptions.AdventurerNotAloneException;
import fr.carbonit.exceptions.ElementOutOfBoundsException;
import fr.carbonit.exceptions.MountainCollisionException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import fr.carbonit.services.validate.ValidateServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MoveServiceImplTest {
    private final Coordinate startAdventurerCoordinate = new Coordinate(0, 0);

    private final OrientationType startAdventurerOrientation = OrientationType.SOUTH;

    private final Size mapSize = new Size(3, 2);

    private MapProperties mapProperties;

    private Adventurer adventurer;

    @Mock
    private ValidateServiceImpl validateService;

    @InjectMocks
    private MoveServiceImpl moveServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        mapProperties = new MapProperties(mapSize);
        adventurer = new Adventurer(startAdventurerCoordinate, "testAdventurer", startAdventurerOrientation);
        mapProperties.getAdventurers().add(adventurer);
    }

    @AfterEach
    void afterEach() {
        assertTrue(adventurer.getMoves().isEmpty());
    }

    @Test
    void testPlayMoves_shouldMoveForward() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 1;
        addMoves(MoveType.FORWARD, nbForward);
        Coordinate expectedCoordinate = new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + nbForward);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldTurnRight() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        addMoves(MoveType.RIGHT, 1);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, never()).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.WEST, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldTurnLeft() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        addMoves(MoveType.LEFT, 1);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, never()).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.EAST, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldTurnBack() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        addMoves(MoveType.RIGHT, 2);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, never()).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.NORTH, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldOpenTreasure() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 1;
        int nbTreasures = 1;
        addMoves(MoveType.FORWARD, nbForward);
        Coordinate expectedCoordinate = new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + nbForward);
        Treasure treasure = new Treasure(expectedCoordinate, nbTreasures);
        mapProperties.getTreasures().add(treasure);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertFalse(mapProperties.getTreasures().contains(treasure));
        assertEquals(nbTreasures, adventurer.getTreasuresCount());
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldOpenOnlyOneTreasure() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 1;
        int nbTreasures = 3;
        addMoves(MoveType.FORWARD, nbForward);
        Coordinate expectedCoordinate = new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + nbForward);
        Treasure treasure = new Treasure(expectedCoordinate, nbTreasures);
        mapProperties.getTreasures().add(treasure);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertTrue(mapProperties.getTreasures().contains(treasure));
        assertEquals(nbTreasures - 1, treasure.getCount());
        assertEquals(1, adventurer.getTreasuresCount());
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldOpenAllTreasures() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbTreasures = 2;
        int nbForward = 3;
        addMoves(MoveType.FORWARD, 2);
        addMoves(MoveType.LEFT, 2);
        addMoves(MoveType.FORWARD, 1);
        Coordinate expectedCoordinate = new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + 1);
        Treasure treasure = new Treasure(expectedCoordinate, nbTreasures);
        mapProperties.getTreasures().add(treasure);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertFalse(mapProperties.getTreasures().contains(treasure));
        assertEquals(nbTreasures, adventurer.getTreasuresCount());
        assertEquals(OrientationType.NORTH, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByMapMaxHeight() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.FORWARD, nbForward);
        Coordinate expectedCoordinate = new Coordinate(startAdventurerCoordinate.getX(), mapSize.getHeight());

        doNothing().doNothing().doThrow(ElementOutOfBoundsException.class).when(validateService).validateAdventurerPosition(mapProperties, adventurer);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByMapMinWidth() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.RIGHT, 1);
        addMoves(MoveType.FORWARD, nbForward);
        doThrow(ElementOutOfBoundsException.class).when(validateService).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.WEST, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByMapMinHeight() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.RIGHT, 2);
        addMoves(MoveType.FORWARD, nbForward);
        doThrow(ElementOutOfBoundsException.class).when(validateService).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.NORTH, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByMapMaxWidth() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.LEFT, 1);
        addMoves(MoveType.FORWARD, nbForward);
        Coordinate expectedCoordinate = new Coordinate(mapSize.getWidth(), startAdventurerCoordinate.getY());

        doNothing().doNothing().doNothing().doThrow(ElementOutOfBoundsException.class).when(validateService).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(OrientationType.EAST, adventurer.getOrientation());
        assertEquals(expectedCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByMountain() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.FORWARD, nbForward);
        Mountain mountain = new Mountain(new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + 1));
        mapProperties.getMountains().add(mountain);

        doThrow(MountainCollisionException.class).when(validateService).validateAdventurerPosition(mapProperties, adventurer);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    @Test
    void testPlayMoves_shouldBeBlockedByAdventurer() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        int nbForward = 5;
        addMoves(MoveType.FORWARD, nbForward);
        Adventurer adventurer2 = new Adventurer(new Coordinate(startAdventurerCoordinate.getX(), startAdventurerCoordinate.getY() + 1), "blockingAdventurer", OrientationType.SOUTH);
        mapProperties.getAdventurers().add(adventurer2);

        doThrow(AdventurerNotAloneException.class).when(validateService).validateAdventurerPosition(mapProperties, adventurer);

        moveServiceImplUnderTest.playMoves(mapProperties);

        verify(validateService, times(nbForward)).validateAdventurerPosition(any(MapProperties.class), any(Adventurer.class));
        assertEquals(startAdventurerOrientation, adventurer.getOrientation());
        assertEquals(startAdventurerCoordinate, adventurer.getCoordinates());
    }

    private void addMoves(MoveType moveType, int count) {
        for (int i = 0; i < count; i++) {
            adventurer.getMoves().add(moveType);
        }
    }
}
