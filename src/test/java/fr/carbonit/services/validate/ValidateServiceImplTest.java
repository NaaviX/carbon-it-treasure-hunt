package fr.carbonit.services.validate;

import fr.carbonit.exceptions.AdventurerNotAloneException;
import fr.carbonit.exceptions.ElementOutOfBoundsException;
import fr.carbonit.exceptions.MountainCollisionException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ValidateServiceImplTest {

    @InjectMocks
    private ValidateServiceImpl validateServiceUnderTest;

    @Test
    void testValidateMap() throws AdventurerNotAloneException, ElementOutOfBoundsException, MountainCollisionException {
        final MapProperties mapProperties = getCorrectMapProperties();

        validateServiceUnderTest.validateMap(mapProperties);
    }

    @Test
    void testValidateMap_ThrowsElementOutOfBoundsException() {
        final MapProperties mapProperties = getOutOfBoundsExceptionMapProperties();

        assertThrows(ElementOutOfBoundsException.class, () -> validateServiceUnderTest.validateMap(mapProperties));
    }

    @Test
    void testValidateMap_ThrowsMountainCollisionException() {
        // Setup
        final MapProperties mapProperties = getMountainCollisionExceptionMapProperties();

        // Run the test
        assertThrows(MountainCollisionException.class, () -> validateServiceUnderTest.validateMap(mapProperties));
    }

    @Test
    void testValidateMap_ThrowsAdventurerNotAloneException() {
        final MapProperties mapProperties = getAdventurerNotAloneExceptionMapProperties();


        assertThrows(AdventurerNotAloneException.class, () -> validateServiceUnderTest.validateMap(mapProperties));
    }

    @Test
    void testValidateAdventurerPosition() throws Exception {
        final MapProperties mapProperties = getCorrectMapProperties();

        validateServiceUnderTest.validateAdventurerPosition(mapProperties, mapProperties.getAdventurers().get(0));
    }

    @Test
    void testValidateAdventurerPosition_ThrowsElementOutOfBoundsException() {
        // Setup
        final MapProperties mapProperties = getOutOfBoundsExceptionMapProperties();

        // Run the test
        assertThrows(ElementOutOfBoundsException.class,
                () -> validateServiceUnderTest.validateAdventurerPosition(mapProperties, mapProperties.getAdventurers().get(0)));
    }

    @Test
    void testValidateAdventurerPosition_ThrowsMountainCollisionException() {
        final MapProperties mapProperties = getMountainCollisionExceptionMapProperties();

        assertThrows(MountainCollisionException.class,
                () -> validateServiceUnderTest.validateAdventurerPosition(mapProperties, mapProperties.getAdventurers().get(0)));
    }

    @Test
    void testValidateAdventurerPosition_ThrowsAdventurerNotAloneException() {
        // Setup
        final MapProperties mapProperties = getAdventurerNotAloneExceptionMapProperties();

        assertThrows(AdventurerNotAloneException.class,
                () -> validateServiceUnderTest.validateAdventurerPosition(mapProperties, mapProperties.getAdventurers().get(0)));
    }

    private MapProperties getCorrectMapProperties() {
        final MapProperties mapProperties = new MapProperties(new Size(1, 1));
        final Adventurer adventurer1 = new Adventurer(new Coordinate(1, 0), "name", OrientationType.NORTH);
        final Adventurer adventurer2 = new Adventurer(new Coordinate(1, 1), "name", OrientationType.NORTH);
        final Mountain mountain = new Mountain(new Coordinate(0, 0));
        final Treasure treasure1 = new Treasure(new Coordinate(1, 0), 2);
        final Treasure treasure2 = new Treasure(new Coordinate(0, 1), 1);
        final Treasure treasure3 = new Treasure(new Coordinate(0, 1), 1);

        mapProperties.getAdventurers().addAll(List.of(adventurer1, adventurer2));
        mapProperties.getMountains().add(mountain);
        mapProperties.getTreasures().addAll(List.of(treasure1, treasure2, treasure3));

        return mapProperties;
    }

    private MapProperties getOutOfBoundsExceptionMapProperties() {
        final MapProperties mapProperties = new MapProperties(new Size(0, 0));
        final Adventurer adventurer = new Adventurer(new Coordinate(1, 0), "name", OrientationType.NORTH);
        mapProperties.getAdventurers().add(adventurer);

        return mapProperties;
    }

    private MapProperties getMountainCollisionExceptionMapProperties() {
        final MapProperties mapProperties = new MapProperties(new Size(1, 1));
        final Adventurer adventurer = new Adventurer(new Coordinate(0, 0), "name", OrientationType.EAST);
        final Mountain mountain = new Mountain(new Coordinate(0, 0));
        adventurer.getMoves().add(MoveType.FORWARD);
        mapProperties.getAdventurers().add(adventurer);
        mapProperties.getMountains().add(mountain);
        return mapProperties;
    }

    private MapProperties getAdventurerNotAloneExceptionMapProperties() {
        final MapProperties mapProperties = new MapProperties(new Size(1, 1));
        Coordinate expectedCoordinate = new Coordinate(0, 0);
        final Adventurer adventurer1 = new Adventurer(expectedCoordinate, "adv1", OrientationType.SOUTH);
        final Adventurer adventurer2 = new Adventurer(expectedCoordinate, "adv2", OrientationType.NORTH);
        mapProperties.getAdventurers().addAll(List.of(adventurer1, adventurer2));

        return mapProperties;
    }
}
