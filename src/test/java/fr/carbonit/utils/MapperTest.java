package fr.carbonit.utils;

import fr.carbonit.exceptions.InvalidInputFileException;
import fr.carbonit.exceptions.MoveNotFoundException;
import fr.carbonit.exceptions.OrientationNotFoundException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import fr.carbonit.services.file.FileService;
import fr.carbonit.services.file.FileServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MapperTest {

    @BeforeEach
    void setup() throws InvalidInputFileException {
        FileService fileService = new FileServiceImpl();
        fileService.getLinesFromFile(IConstants.TEST_INPUT_FILE_PATH);
    }

    @Test
    void testMapLineToAdventurer() throws Exception {
        String[] mapLine = "A - Lara - 1 - 1 - S - AADADAGGA".split(IConstants.LINE_DELIMITER);
        Adventurer adventurer = new Adventurer(new Coordinate(1, 1), "Lara", OrientationType.SOUTH);
        adventurer.getMoves().addAll(List.of(MoveType.FORWARD, MoveType.FORWARD, MoveType.RIGHT, MoveType.FORWARD, MoveType.RIGHT, MoveType.FORWARD, MoveType.LEFT, MoveType.LEFT, MoveType.FORWARD));

        final Adventurer result = Mapper.mapLineToAdventurer(mapLine);

        assertEquals(result, adventurer);
    }

    @Test
    void testMapLineToAdventurer_ThrowsOrientationNotFoundException() {
        String[] mapLine = "A - Lara - 1 - 1 - X - AADADAGGA".split(IConstants.LINE_DELIMITER);

        assertThrows(OrientationNotFoundException.class, () -> Mapper.mapLineToAdventurer(mapLine));
    }

    @Test
    void testMapLineToAdventurer_ThrowsMoveNotFoundException() {
        String[] mapLine = "A - Lara - 1 - 1 - S - AADADAGGT".split(IConstants.LINE_DELIMITER);

        assertThrows(MoveNotFoundException.class, () -> Mapper.mapLineToAdventurer(mapLine));
    }

    @Test
    void testMapLineToMountain() {
        String[] mapLine = "M - 2 - 1".split(IConstants.LINE_DELIMITER);

        Mountain mountain = new Mountain(new Coordinate(2, 1));

        final Mountain result = Mapper.mapLineToMountain(mapLine);

        assertEquals(result, mountain);
    }

    @Test
    void testMapLineToTreasure() {
        String[] mapLine = "T - 1 - 3 - 3".split(IConstants.LINE_DELIMITER);
        // Setup
        final Treasure expectedResult = new Treasure(new Coordinate(1, 3), 3);

        // Run the test
        final Treasure result = Mapper.mapLineToTreasure(mapLine);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testMapLineToMapProperties() {
        String[] mapLine = "C - 3 - 4".split(IConstants.LINE_DELIMITER);

        final MapProperties expectedResult = new MapProperties(new Size(3, 4));

        final MapProperties result = Mapper.mapLineToMapProperties(mapLine);

        assertEquals(expectedResult, result);
    }
}
