package fr.carbonit.utils;

import fr.carbonit.exceptions.MoveNotFoundException;
import fr.carbonit.models.*;
import fr.carbonit.models.enums.MoveType;
import fr.carbonit.models.enums.OrientationType;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UtilsTest {

    @Test
    void testFormatLine() throws MoveNotFoundException {
        String moves = "AADADAGGA";
        Adventurer adventurer = new Adventurer(new Coordinate(1, 1), "Lara", OrientationType.SOUTH);
        adventurer.getMoves().addAll(Utils.getMovesFromString(moves));
        assertEquals("A - Lara - 1 - 1 - S - ".concat(moves), Utils.formatLine(adventurer));
    }

    @Test
    void testGetMovesFromString() throws MoveNotFoundException {
        assertEquals(List.of(MoveType.FORWARD, MoveType.RIGHT, MoveType.LEFT), Utils.getMovesFromString("ADG"));
        assertThrows(MoveNotFoundException.class, () -> Utils.getMovesFromString("ADX"));
    }

    @Test
    void testGetAllElements() throws MoveNotFoundException {
        final MapProperties mapProperties = new MapProperties(new Size(1, 1));
        Adventurer adventurer = new Adventurer(new Coordinate(0, 0), "name", OrientationType.SOUTH);
        adventurer.getMoves().addAll(Utils.getMovesFromString("AADGA"));
        mapProperties.getAdventurers().add(adventurer);
        Mountain mountain = new Mountain(new Coordinate(1, 1));
        mapProperties.getMountains().add(mountain);
        Treasure treasure = new Treasure(new Coordinate(1, 0), 2);
        mapProperties.getTreasures().add(treasure);


        final List<Element> expectedResult = List.of(adventurer, mountain, treasure);

        final List<Element> result = Utils.getAllElements(mapProperties);

        assertEquals(expectedResult, result);
    }
}
